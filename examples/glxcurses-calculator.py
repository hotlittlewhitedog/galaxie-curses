#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXCurses
import curses
import logging


if __name__ == '__main__':
    logging.basicConfig(filename='/tmp/galaxie-curses.log',
                        level=logging.DEBUG,
                        format='%(asctime)s, %(levelname)s, %(message)s')
    logging.info('Started glxcurses-demo')

    # Create the main Application
    app = GLXCurses.Application()
    app.set_name('GLXCurses Calculator')

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.app_info_label = app.get_name()

    # The main entry
    entrybuffer = GLXCurses.EntryBuffer()

    entry = GLXCurses.Entry()
    entry.set_buffer(entrybuffer)
    entry.set_text("0")

    # Button
    button_0 = GLXCurses.Button()
    button_0.set_text('0')

    button_1 = GLXCurses.Button()
    button_1.set_text('1')

    button_2 = GLXCurses.Button()
    button_2.set_text('2')

    button_3 = GLXCurses.Button()
    button_3.set_text('3')

    button_4 = GLXCurses.Button()
    button_4.set_text('4')

    button_5 = GLXCurses.Button()
    button_5.set_text('5')

    button_6 = GLXCurses.Button()
    button_6.set_text('6')

    button_7 = GLXCurses.Button()
    button_7.set_text('7')

    button_8 = GLXCurses.Button()
    button_8.set_text('8')

    button_9 = GLXCurses.Button()
    button_9.set_text('9')

    button_division = GLXCurses.Button()
    button_division.set_text('/')

    button_multiply = GLXCurses.Button()
    button_multiply.set_text('x')

    button_subtract = GLXCurses.Button()
    button_subtract.set_text('-')

    button_add = GLXCurses.Button()
    button_add.set_text('+')

    button_dot = GLXCurses.Button()
    button_dot.set_text('.')

    button_percent = GLXCurses.Button()
    button_percent.set_text('%')

    # Create a new Horizontal Box container

    hbox_line1 = GLXCurses.HBox()
    hbox_line1.set_spacing(0)
    hbox_line1.pack_end(button_7)
    hbox_line1.pack_end(button_8)
    hbox_line1.pack_end(button_9)
    hbox_line1.pack_end(button_division)

    hbox_line2 = GLXCurses.HBox()
    hbox_line2.set_spacing(0)
    hbox_line2.pack_end(button_4)
    hbox_line2.pack_end(button_5)
    hbox_line2.pack_end(button_6)
    hbox_line2.pack_end(button_multiply)

    hbox_line3 = GLXCurses.HBox()
    hbox_line3.set_spacing(0)
    hbox_line3.pack_end(button_1)
    hbox_line3.pack_end(button_2)
    hbox_line3.pack_end(button_3)
    hbox_line3.pack_end(button_subtract)

    hbox_line4 = GLXCurses.HBox()
    hbox_line4.set_spacing(0)
    hbox_line4.pack_end(button_0)
    hbox_line4.pack_end(button_dot)
    hbox_line4.pack_end(button_percent)
    hbox_line4.pack_end(button_add)

    # Create a Horizontal Separator and a Label
    hline = GLXCurses.HSeparator()

    label_press_q = GLXCurses.Label()
    label_press_q.set_text('Press "q" key to exit ... What about you arrows\'s key\'s')
    label_press_q.set_single_line_mode(True)
    label_press_q.set_justify('center')
    label_press_q.set_alignment(0.5, 0.3)
    label_press_q.override_color('yellow')

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()
    vbox_main.pack_end(entry)
    vbox_main.pack_end(hbox_line1)
    vbox_main.pack_end(hbox_line2)
    vbox_main.pack_end(hbox_line3)
    vbox_main.pack_end(hbox_line4)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.add(vbox_main)

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    signal_context_id = statusbar.get_context_id("SIGNAL")
    curses_context_id = statusbar.get_context_id("CURSES")


    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, 'HANDLE KEY: ' + str(event_args[0]))

        if app.get_is_focus()['widget'] is None:

            if event_args[0] == curses.KEY_F1:
                pass
            if event_args[0] == curses.KEY_F2:
                entry.set_visibility(not entry.visibility)
            if event_args[0] == curses.KEY_F3:
                entry.set_can_focus(not entry.get_can_focus())
                entry.set_editable(not entry.get_editable())
            if event_args[0] == curses.KEY_F4:
                pass
            if event_args[0] == curses.KEY_F5:
                pass
            if event_args[0] == curses.KEY_F6:
                pass
            if event_args[0] == curses.KEY_F7:
                pass
            if event_args[0] == curses.KEY_F8:
                pass
            if event_args[0] == curses.KEY_F9:
                pass
            if event_args[0] == curses.KEY_F10:
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.quit()

            if event_args[0] == curses.KEY_UP:
                x, y = label_press_q.get_alignment()
                y -= 0.033
                label_press_q.set_alignment(x, y)

            if event_args[0] == curses.KEY_DOWN:
                x, y = label_press_q.get_alignment()
                y += 0.033
                label_press_q.set_alignment(x, y)

            if event_args[0] == curses.KEY_RIGHT:
                x, y = label_press_q.get_alignment()
                x += 0.033
                label_press_q.set_alignment(x, y)
            if event_args[0] == curses.KEY_LEFT:
                x, y = label_press_q.get_alignment()
                x -= 0.033
                label_press_q.set_alignment(x, y)

            # Keyboard temporary thing
            if event_args[0] == ord('q'):
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.quit()


    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()
        if event_args['id'] == entry.get_widget_id():
            statusbar.remove_all(signal_context_id)
            statusbar.push(signal_context_id, event_args['name'] + ' ' + event_signal)


    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )


    # Add Everything inside the Application
    app.add_menubar(menu)
    app.add_window(win_main)
    app.add_statusbar(statusbar)

    # Event's and Signals
    app.connect('BUTTON1_CLICKED', on_click)  # Mouse Button
    #app.connect('BUTTON1_RELEASED', on_click)  # Mouse Button
    #app.connect('MOUSE_EVENT', on_click)  # Mouse Button
    app.connect('CURSES', handle_keys)  # Keyboard
    app.connect('SIGNALS', signal_event)  # Something it emit a signal

    # Main loop
    GLXCurses.mainloop.run()

    # THE END
    sys.exit(0)
