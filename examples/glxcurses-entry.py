#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved
import os
import sys

# Require when you haven't GLXCurses as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXCurses
import curses
import logging


if __name__ == '__main__':
    logging.basicConfig(filename='/tmp/galaxie-curses.log',
                        level=logging.DEBUG,
                        format='%(asctime)s, %(levelname)s, %(message)s')
    logging.info('Started glxcurses-demo')

    # Create the main Application
    app = GLXCurses.Application()
    app.set_name('GLXCurses Entry Demo')

    # Creat a Status Bar
    toolbar = GLXCurses.ToolBar()
    toolbar.button_list = [
        'Help',
        'Visibility',
        'Can Focus',
        'Prelight',
        'Selected',
        'Insensitive',
        '',
        '',
        'Menu',
        'Quit'
    ]

    # Create a Menu
    menu = GLXCurses.MenuBar()
    menu.app_info_label = app.get_name()

    entrybuffer_login = GLXCurses.EntryBuffer()
    entrybuffer_password = GLXCurses.EntryBuffer()

    # Create a Entry thing
    entry_login = GLXCurses.Entry()
    entry_login.set_buffer(entrybuffer_login)
    entry_login.set_text("Here you login")
    entry_login.set_completion(GLXCurses.EntryCompletion())
    #Entry1.set_justify('CENTER')
    #Entry1.set_alignment(1.0)

    entry_password = GLXCurses.Entry()
    entry_password.set_buffer(entrybuffer_password)
    entry_password.set_text("Here you password")
    entry_password.set_visibility(False)
    entry_password.grab_focus_without_selecting()
    #Entry1.set_justify('CENTER')
    #Entry1.set_alignment(1.0)

    label_login = GLXCurses.Label()
    label_login.set_text('Login:')
    label_login.set_single_line_mode(True)
    #Label_Login.set_justify('CENTER')
    label_login.set_alignment(1.0, 0.6)

    label_password = GLXCurses.Label()
    label_password.set_text('Password:')
    label_password.set_single_line_mode(True)
    #Label_Password.set_justify('CENTER')
    label_password.set_alignment(1.0, 0.6)

    # Create a new Horizontal Box container
    hbox_login = GLXCurses.HBox()
    hbox_login.set_spacing(0)

    hbox_login.pack_start(label_login)
    hbox_login.pack_end(entry_login)

    hbox_password = GLXCurses.HBox()
    hbox_password.set_spacing(0)

    hbox_password.pack_start(label_password)
    hbox_password.pack_end(entry_password)

    # Create a Horizontal Separator and a Label
    hline = GLXCurses.HSeparator()

    label_press_q = GLXCurses.Label()
    label_press_q.set_text('Press "q" key to exit ... What about you arrows\'s key\'s')
    label_press_q.set_single_line_mode(True)
    label_press_q.set_justify('center')
    label_press_q.set_alignment(0.5, 0.3)
    label_press_q.override_color('yellow')

    # Create a main Vertical Box
    vbox_main = GLXCurses.VBox()
    vbox_main.pack_end(hline)
    vbox_main.pack_end(hbox_login)
    vbox_main.pack_end(hbox_password)
    vbox_main.pack_end(hline)
    vbox_main.pack_end(label_press_q)

    # Create the main Window
    win_main = GLXCurses.Window()
    win_main.add(vbox_main)

    # Create a Status Bar
    statusbar = GLXCurses.StatusBar()
    signal_context_id = statusbar.get_context_id("SIGNAL")
    curses_context_id = statusbar.get_context_id("CURSES")


    def handle_keys(self, event_signal, *event_args):
        statusbar.remove_all(curses_context_id)
        statusbar.push(curses_context_id, 'HANDLE KEY: ' + str(event_args[0]))

        if app.get_is_focus()['widget'] is None:

            if event_args[0] == curses.KEY_F1:
                pass
            if event_args[0] == curses.KEY_F2:
                entry_password.set_visibility(not entry_password.visibility)
            if event_args[0] == curses.KEY_F3:
                entry_password.set_can_focus(not entry_password.get_can_focus())
                entry_password.set_editable(not entry_password.get_editable())
            if event_args[0] == curses.KEY_F4:
                pass
            if event_args[0] == curses.KEY_F5:
                pass
            if event_args[0] == curses.KEY_F6:
                pass
            if event_args[0] == curses.KEY_F7:
                pass
            if event_args[0] == curses.KEY_F8:
                pass
            if event_args[0] == curses.KEY_F9:
                pass
            if event_args[0] == curses.KEY_F10:
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.quit()

            if event_args[0] == curses.KEY_UP:
                x, y = label_press_q.get_alignment()
                y -= 0.033
                label_press_q.set_alignment(x, y)

            if event_args[0] == curses.KEY_DOWN:
                x, y = label_press_q.get_alignment()
                y += 0.033
                label_press_q.set_alignment(x, y)

            if event_args[0] == curses.KEY_RIGHT:
                x, y = label_press_q.get_alignment()
                x += 0.033
                label_press_q.set_alignment(x, y)
            if event_args[0] == curses.KEY_LEFT:
                x, y = label_press_q.get_alignment()
                x -= 0.033
                label_press_q.set_alignment(x, y)

            # Keyboard temporary thing
            if event_args[0] == ord('q'):
                # Everything have a end, the main loop too ...
                GLXCurses.mainloop.quit()


    def on_click(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()
        if event_args['id'] == entry_login.get_widget_id():
            statusbar.remove_all(signal_context_id)
            statusbar.push(signal_context_id, event_args['name'] + ' ' + event_signal)


    def signal_event(self, event_signal, event_args=None):
        if event_args is None:
            event_args = dict()

        # Crash AUTO
        # statusbar.push(
        #    signal_context_id, "{0}: {1}".format(event_signal, event_args)
        # )


    # Add Everything inside the Application
    app.add_menubar(menu)
    app.add_window(win_main)
    # app.remove_window(win_main)
    app.add_statusbar(statusbar)
    app.add_toolbar(toolbar)

    # Event's and Signals
    app.connect('BUTTON1_CLICKED', on_click)  # Mouse Button
    #app.connect('BUTTON1_RELEASED', on_click)  # Mouse Button
    #app.connect('MOUSE_EVENT', on_click)  # Mouse Button
    app.connect('CURSES', handle_keys)  # Keyboard
    app.connect('SIGNALS', signal_event)  # Something it emit a signal

    # Main loop
    GLXCurses.mainloop.run()

    # THE END
    sys.exit(0)
