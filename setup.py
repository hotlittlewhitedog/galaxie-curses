# -*- coding:utf-8 -*-
import os

from setuptools import setup, find_packages


def _requires_from_file(filename):
    return open(filename).read().splitlines()


# version
here = os.path.dirname(os.path.abspath(__file__))
version = next(
    (
        line.split('=')[1].strip().replace("'", '')
        for line in open(os.path.join(here, 'GLXCurses', '__init__.py'))
        if line.startswith('__version__ = ')
    ), '0.0.dev0'
)

setup(name='galaxie-curses',
      version=version,
      description='Galaxie Curses is a free software ToolKit for the NCurses API',
      url='https://gitlab.com/Tuuux/galaxie-curses',
      author='Tuuux',
      author_email='tuxa@rtnp.org',
      install_requires=_requires_from_file('requirements.txt'),
      license='GPL v3',
      packages=['GLXCurses'],
      zip_safe=False)
