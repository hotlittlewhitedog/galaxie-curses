#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Curses Team, all rights reserved

import unittest

from GLXCurses import Widget
from GLXCurses import Window
from GLXCurses import Object
from GLXCurses import HBox
from GLXCurses.Utils import glxc_type


# Unittest
class TestWidget(unittest.TestCase):

    # Test
    def test_glxc_type(self):
        """Test Widget Type"""
        widget = Widget()
        self.assertTrue(glxc_type(widget))

    def test_Widget_new(self):
        """Test Widget.new()"""
        widget = Widget().new()
        old_id = widget.get_widget_id()
        widget = Widget().new()
        self.assertNotEqual(old_id, widget.get_widget_id())

    def test_Widget_destroy(self):
        """Test Widget.destroy()"""
        widget1 = Widget().new()
        widget2 = Widget().new()
        # check default setting
        widget2.set_parent(widget1)
        self.assertEqual(widget1.in_destruction(widget=widget2), False)
        self.assertEqual(widget2.parent, widget1)

        widget2.destroy()
        self.assertEqual(widget1.in_destruction(widget=widget2), True)
        self.assertEqual(widget2.parent, None)

        # check errors
        # widget is not GLXCurses type
        self.assertRaises(TypeError, widget1.destroy, widget=42)
        # widget is not a GLXC.Widget
        obj = Object()
        self.assertRaises(TypeError, widget1.destroy, widget=obj)

    def test_Widget_in_destruction(self):
        """Test Widget.in_destruction()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.in_destruction(), False)
        # Make test
        widget.flags['IN_DESTRUCTION'] = True
        self.assertEqual(widget.in_destruction(), True)

        # check errors
        # widget is not GLXCurses type
        self.assertRaises(TypeError, widget.in_destruction, widget=42)
        # widget is not a GLXC.Widget
        obj = Object()
        self.assertRaises(TypeError, widget.in_destruction, widget=obj)

    def test_Widget_destroyed(self):
        """Test Widget.destroyed()"""
        widget = Widget()
        # check default value
        self.assertTrue(glxc_type(widget))
        self.assertEqual(isinstance(widget, Widget), True)

        widget.destroyed(widget_pointer=widget)
        self.assertFalse(glxc_type(widget))
        self.assertEqual(isinstance(widget, Widget), True)

        # check errors
        obj = Object()
        widget = Widget()
        # widget is not GLXCurses type
        self.assertRaises(TypeError, widget.destroyed, widget=42, widget_pointer=widget)
        # widget is not a GLXC.Widget
        self.assertRaises(TypeError, widget.destroyed, widget=obj, widget_pointer=widget)
        # widget_pointer is not GLXCurses type
        self.assertRaises(TypeError, widget.destroyed, widget=widget, widget_pointer=42)
        # widget_pointer is not a GLXC.Widget
        self.assertRaises(TypeError, widget.destroyed, widget=widget, widget_pointer=obj)

    def test_Widget_unparent(self):
        """Test Widget.unparent()"""
        # Check error
        obj = Object()
        widget = Widget()
        # widget is not GLXCurses type
        self.assertRaises(TypeError, widget.unparent, widget=42)
        # widget is not a GLXC.Widget
        self.assertRaises(TypeError, widget.unparent, widget=obj)

    def test_Widget_show(self):
        """Test Widget.show()"""
        widget = Widget()
        # check default value
        self.assertFalse(widget.flags['TOPLEVEL'])
        self.assertFalse(widget.flags['REALIZED'])
        self.assertFalse(widget.flags['MAPPED'])
        self.assertFalse(widget.flags['VISIBLE'])
        # test 1
        widget.show()
        self.assertFalse(widget.flags['TOPLEVEL'])
        self.assertFalse(widget.flags['REALIZED'])
        self.assertFalse(widget.flags['MAPPED'])
        self.assertTrue(widget.get_flags()['VISIBLE'])
        # test 2
        widget.flags['TOPLEVEL'] = True
        widget.show()
        self.assertTrue(widget.flags['TOPLEVEL'])
        self.assertTrue(widget.flags['REALIZED'])
        self.assertTrue(widget.flags['MAPPED'])
        self.assertTrue(widget.get_flags()['VISIBLE'])

    def test_Widget_show_now(self):
        """Test Widget.show()"""
        widget = Widget()
        # check default value
        self.assertFalse(widget.flags['REALIZED'])
        self.assertFalse(widget.flags['MAPPED'])
        self.assertFalse(widget.flags['VISIBLE'])
        # test 1
        widget.show_now()
        self.assertTrue(widget.flags['REALIZED'])
        self.assertTrue(widget.flags['MAPPED'])
        self.assertTrue(widget.get_flags()['VISIBLE'])

    def test_Widget_show_all(self):
        """Test Widget.show_all()"""
        window = Window()
        window.flags['TOPLEVEL'] = True
        hobox1 = HBox()
        hobox2 = HBox()
        hobox3 = HBox()

        hobox1.pack_end(hobox2)
        hobox1.pack_end(hobox3)
        window.add(hobox1)

        # check default value
        self.assertTrue(window.flags['TOPLEVEL'])
        self.assertFalse(window.flags['REALIZED'])
        self.assertFalse(window.flags['MAPPED'])
        self.assertFalse(window.flags['VISIBLE'])

        self.assertFalse(hobox1.flags['TOPLEVEL'])
        self.assertFalse(hobox1.flags['REALIZED'])
        self.assertFalse(hobox1.flags['MAPPED'])
        self.assertFalse(hobox1.flags['VISIBLE'])

        self.assertFalse(hobox2.flags['TOPLEVEL'])
        self.assertFalse(hobox2.flags['REALIZED'])
        self.assertFalse(hobox2.flags['MAPPED'])
        self.assertFalse(hobox2.flags['VISIBLE'])

        self.assertFalse(hobox3.flags['TOPLEVEL'])
        self.assertFalse(hobox3.flags['REALIZED'])
        self.assertFalse(hobox3.flags['MAPPED'])
        self.assertFalse(hobox3.flags['VISIBLE'])
        #
        # test 1
        window.show_all()
        self.assertTrue(window.flags['TOPLEVEL'])
        self.assertTrue(window.flags['REALIZED'])
        self.assertTrue(window.flags['MAPPED'])
        self.assertTrue(window.get_flags()['VISIBLE'])

        self.assertFalse(hobox1.flags['TOPLEVEL'])
        self.assertFalse(hobox1.flags['REALIZED'])
        self.assertFalse(hobox1.flags['MAPPED'])
        # self.assertTrue(hobox1.flags['VISIBLE'])

        self.assertFalse(hobox2.flags['TOPLEVEL'])
        self.assertFalse(hobox2.flags['REALIZED'])
        self.assertFalse(hobox2.flags['MAPPED'])
        # self.assertTrue(hobox2.flags['VISIBLE'])

        self.assertFalse(hobox3.flags['TOPLEVEL'])
        self.assertFalse(hobox3.flags['REALIZED'])
        self.assertFalse(hobox3.flags['MAPPED'])
        # self.assertTrue(hobox3.flags['VISIBLE'])

    def test_Widget_map(self):
        """Test Widget.map()"""
        widget = Widget()
        # check default
        self.assertFalse(widget.flags['MAPPED'])
        # test 1
        widget.map()
        self.assertTrue(widget.flags['MAPPED'])
        # check errors
        # widget is not GLXCurses type
        self.assertRaises(TypeError, widget.unmap, widget=42)
        # widget is not a GLXC.Editable
        obj = Object()
        self.assertRaises(TypeError, widget.unmap, widget=obj)

    def test_Widget_unmap(self):
        """Test Widget.unmap()"""
        widget = Widget()
        # check default
        widget.flags['MAPPED'] = True
        self.assertTrue(widget.flags['MAPPED'])
        # test 1
        widget.unmap()
        self.assertFalse(widget.flags['MAPPED'])
        # check errors
        # widget is not GLXCurses type
        self.assertRaises(TypeError, widget.unmap, widget=42)
        # widget is not a GLXC.Editable
        obj = Object()
        self.assertRaises(TypeError, widget.unmap, widget=obj)

    def test_Widget_set_name(self):
        """Test Widget.set_name()"""
        widget = Widget()
        self.assertEqual(widget.name, widget.__class__.__name__)

        widget.set_name("Hello")
        self.assertEqual(widget.name, "Hello")

        widget.set_name()
        self.assertEqual(widget.name, widget.__class__.__name__)

        self.assertRaises(TypeError, widget.set_name, int(42))

    def test_Widget_get_name(self):
        """Test Widget.get_name()"""
        widget = Widget()
        self.assertEqual(widget.get_name(), widget.__class__.__name__)

        widget.name = "Hello"
        self.assertEqual(widget.get_name(), "Hello")

    def test_Widget_set_can_default(self):
        """Test Widget.set_can_default()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.flags['CAN_DEFAULT'], False)
        self.assertEqual(widget.can_default, False)
        # test 1
        widget.set_can_default(True)
        self.assertEqual(widget.flags['CAN_DEFAULT'], True)
        self.assertEqual(widget.can_default, True)
        # check error
        self.assertRaises(TypeError, widget.set_can_default, int(42))

    def test_Widget_get_can_default(self):
        """Test Widget.set_can_default()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.get_can_default(), False)
        # test 1
        widget.flags['CAN_DEFAULT'] = True
        self.assertEqual(widget.get_can_default(), True)

    def test_Widget_set_can_focus(self):
        """Test Widget.set_can_focus()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.flags['CAN_FOCUS'], False)
        self.assertEqual(widget.can_focus, False)
        # test 1
        widget.set_can_focus(True)
        self.assertEqual(widget.flags['CAN_FOCUS'], True)
        self.assertEqual(widget.can_focus, True)
        # check error
        self.assertRaises(TypeError, widget.set_can_focus, int(42))

    def test_Widget_get_can_focus(self):
        """Test Widget.get_can_focus()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.get_can_focus(), False)
        # test 1
        widget.flags['CAN_FOCUS'] = True
        self.assertEqual(widget.get_can_focus(), True)

    def test_Widget_set_focus_on_click(self):
        """Test Widget.set_focus_on_click()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.flags['FOCUS_ON_CLICK'], True)
        self.assertEqual(widget.focus_on_click, True)
        # test 1
        widget.set_focus_on_click(False)
        self.assertEqual(widget.flags['FOCUS_ON_CLICK'], False)
        self.assertEqual(widget.focus_on_click, False)
        # check error
        self.assertRaises(TypeError, widget.set_focus_on_click, int(42))

    def test_Widget_get_focus_on_click(self):
        """Test Widget.get_focus_on_click()"""
        widget = Widget()
        # check default value
        self.assertEqual(widget.get_focus_on_click(), True)
        # test 1
        widget.flags['FOCUS_ON_CLICK'] = False
        self.assertEqual(widget.get_focus_on_click(), False)

    def test_Widget_get_double_buffered(self):
        """Test Widget.get_double_buffered()"""
        widget = Widget()
        self.assertRaises(NotImplementedError, widget.get_double_buffered)

    def test_Widget_set_double_buffered(self):
        """Test Widget.set_double_buffered()"""
        widget = Widget()
        self.assertRaises(NotImplementedError, widget.set_double_buffered)
